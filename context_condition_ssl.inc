<?php

/**
 * @file
 * Defines a class for a Context condition for SSL
 */

/**
 * Expose the ssl field value as a context condition.
 */
class context_condition_ssl extends context_condition {
  /**
   * Condition values.
   */
  function condition_values() {
    return array(
      'http' => t('HTTP'),
      'https' => t('HTTPS'),
    );
  }

  /**
   * Execute.
   */
  function execute() {
    foreach ($this->get_contexts() as $context) {
      $values = $this->fetch_from_context($context, 'values');
      $ssl_on = context_condition_ssl_enabled();
      foreach ($values as $value => $label) {
        if (($ssl_on && $value == 'https') || (!$ssl_on && $value == 'http')) {
          $this->condition_met($context, $value);
        }
      }
    }
  }
}
