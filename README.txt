INSTALLATION:
- Enable the module

USAGE:
- Create a new Context rule
- Add the "SSL" condition
- Select the appropriate "HTTP" or "HTTPS" condition value for your rule
- Select the "Require all conditions" checkbox if you have other conditions
